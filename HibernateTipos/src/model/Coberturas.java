/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author toure19_notebook
 */
public class Coberturas implements Serializable {

    private int oftamologia;
    private int dental;
    private int fecundacionInVitro;

    public Coberturas() {
    }

    public Coberturas(int oftamologia, int dental, int fecundacionInVitro) {
        this.oftamologia = oftamologia;
        this.dental = dental;
        this.fecundacionInVitro = fecundacionInVitro;
    }

    public int getOftamologia() {
        return oftamologia;
    }

    public void setOftamologia(int oftamologia) {
        this.oftamologia = oftamologia;
    }

    public int getDental() {
        return dental;
    }

    public void setDental(int dental) {
        this.dental = dental;
    }

    public int getFecundacionInVitro() {
        return fecundacionInVitro;
    }

    public void setFecundacionInVitro(int fecundacionInVitro) {
        this.fecundacionInVitro = fecundacionInVitro;
    }

}
