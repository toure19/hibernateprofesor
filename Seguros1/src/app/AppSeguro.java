/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.Date;
import model.Seguro;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author toure19_notebook
 */
public class AppSeguro {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //CREAMOS CONEXION
        SessionFactory sessionFactory;
        Configuration configuration = new Configuration();
        configuration.configure();
        //configuration.addClass(model.Profesor.class);
        //configuration.addResource("model/Profesor.hbm.xml");
        sessionFactory = configuration.buildSessionFactory();

        //CREAMOS UN OBJETO
        Date date = new Date();
        Seguro seguro = new Seguro(1, "12345678A", "AITOR", "BENAVENT", "APARICIO", 26, 0, date);

        //CREAMOS UNA SESSION
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        //GUARDAR OBJETO
        session.save(seguro);
        session.getTransaction().commit();

        //MODIFICAMOS EL OBJETO
        seguro.setNif("87654321A");
        seguro.setNombre("PEPE");
        session.beginTransaction();
        session.saveOrUpdate(seguro);
        session.getTransaction().commit();

        //CREAMOS EL SEGUNDO OBJETO
        Seguro seguro2 = new Seguro(8, "11111111E", "PACO", "FELIPE", "SANCHIS", 22, 12, date);
        session.beginTransaction();
        session.save(seguro2);
        session.getTransaction().commit();

        //BORRAMOS EL SEGUNDO OBJETO
        session.beginTransaction();
        session.delete(seguro2);
        session.getTransaction().commit();

        //CERRAR CONEXIÓN
        session.close();
        sessionFactory.close();
    }

}
