/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.List;
import model.Profesor;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author toure19_notebook
 */
public class AppProfesor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //CREAMOS CONEXION
        SessionFactory sessionFactory;
        Configuration configuration = new Configuration();
        configuration.configure();
        //configuration.addClass(model.Profesor.class);
        //configuration.addResource("model/Profesor.hbm.xml");
        sessionFactory = configuration.buildSessionFactory();

        //CREAMOS UNA SESSION
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("SELECT p FROM Profesor p");
        Query query2 = session.createQuery("SELECT p.id,p.nombre FROM Profesor p");

        List<Profesor> profesores = query.list();
        List<Object[]> datos = query2.list();

        for (Profesor p : profesores) {
            System.out.println(p.toString());
        }

        for (Object[] obj : datos) {
            System.out.println(obj[0]);
            System.out.println(obj[1]);
        }

        session.close();
        sessionFactory.close();
    }

}
